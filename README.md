# OpenAuth for Openset 

### System requirements

- ** Java 11 ** 
- ** Maven **
- ** MySQL **

In src/main/resources/application.properties you can set your properties

## Parameters needed for run the Server
1. **server.port** is the port that the HTTP server must listen to
2. **spring.datasource.url** is the JDBC driver URI (i.e. jdbc:mysql://localhost:3306/openset_auth?serverTimezone=Europe/Rome)
3. **spring.datasource.username** is the db username  (i.e. root)
4. **spring.datasource.password** is the db password 
5. **jwt.security.secret** is the SECRET KEY for JWT 
6. **jwt.security.strategy** is the Algorithm that is used to sign JWT (you can choose: HS256 for HMAC256, HS384 for HMAC384, HS512 for HMAC512)


### Setting up a local istance
1. Clone this repository 
2. Start a RDBMS like MySQL or similar
3. Create a DB for production and DB for tests (i.e. openset_auth and openset_auth_test)
4. You can change properties in src/main/resources/application.properties for main application
5. You can change properties in src/test/resources/application-test.properties for test application
6. From project’s root directory, run `mvn install && java -jar target/*.jar`


