package com.openset.openauth.jwt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;

import com.auth0.jwt.algorithms.Algorithm;

@SpringBootTest
@TestPropertySource(properties = {"jwt.security.strategy=HS256", "jwt.security.secret=secret"})
public class AlgorithmConfigBeansTest {
	
	@Autowired
	private AlgorithmConfigurer algorithmConfigurer;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Test
	public void whenStrategyIsHS256_thenReturnIstanceOfHMAC256() {
		Algorithm algorithm = applicationContext.getBean(Algorithm.class);
		String algorithmClassExpect = "class com.auth0.jwt.algorithms.HMACAlgorithm";
		String nameAlgExcept = "HS256";
		assertEquals(algorithmClassExpect, algorithm.getClass().toString());
		assertEquals(nameAlgExcept, algorithm.getName());
	}
	
	@Test
	public void whenStrategyIsHS384_thenReturnIstanceOfHMAC384() {
		Algorithm algorithm = new AlgorithmConfigurer().concreteAlgorithm("HS384","secretkey");
		String algorithmClassExpect = "class com.auth0.jwt.algorithms.HMACAlgorithm";
		String nameAlgExcept = "HS384";
		assertEquals(algorithmClassExpect, algorithm.getClass().toString());
		assertEquals(nameAlgExcept, algorithm.getName());
	}
	
	@Test
	public void whenStrategyIsHS512_thenReturnIstanceOfHMAC512() {
		Algorithm algorithm = new AlgorithmConfigurer().concreteAlgorithm("HS512","secretkey");
		String algorithmClassExpect = "class com.auth0.jwt.algorithms.HMACAlgorithm";
		String nameAlgExcept = "HS512";
		assertEquals(algorithmClassExpect, algorithm.getClass().toString());
		assertEquals(nameAlgExcept, algorithm.getName());
	}
	
	@Test
	public void whenStrategyIsWrongOrDefault_thenReturnIstanceOfHMAC256() {
		Algorithm algorithm = new AlgorithmConfigurer().concreteAlgorithm("default","secretkey");
		String algorithmClassExpect = "class com.auth0.jwt.algorithms.HMACAlgorithm";
		String nameAlgExcept = "HS256";
		assertEquals(algorithmClassExpect, algorithm.getClass().toString());
		assertEquals(nameAlgExcept, algorithm.getName());
	}
	
}
