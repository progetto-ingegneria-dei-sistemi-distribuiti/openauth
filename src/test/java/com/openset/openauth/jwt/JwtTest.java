package com.openset.openauth.jwt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.openset.openauth.jwt.JwtUtil;

@SpringBootTest
public class JwtTest {

	@Autowired
	JwtUtil jwtUtil;

	@Test
	public void equalsSubjectAndDateExp_equalsToken() {
		Date date = new Date(); // now
		String token01 = jwtUtil.getToken("hello", date);
		String token02 = jwtUtil.getToken("hello", date);
		assertTrue(token01.equals(token02));
	}

	@Test // Unit test
	public void whenGiveACorrectToken_thenReturnSubject() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method method = getMethodPrivateToMethodPublic("getSubject", String.class);
		String token = jwtUtil.getToken("hello");
		String subjectFromMethod = null;
		subjectFromMethod = (String) method.invoke(jwtUtil, token);
	
		assertTrue("hello".equals(subjectFromMethod));
	}

	@Test // Unit test
	public void whenGiveInvalidToken_thenReturnNullSubject() {
		Method method = getMethodPrivateToMethodPublic("getSubject", String.class);
		String token = jwtUtil.getToken("hello");
		token = token.substring(1, token.length() - 2);
		String subjectFromMethod = null;
		try {
			subjectFromMethod = (String) method.invoke(jwtUtil, token); // prevent InvocationTargetException for
																		// null pointer
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.getCause().printStackTrace();
		}
		assertNull(subjectFromMethod);
	}

	@Test // Unit test
	public void whenGiveACorrectToken_thenReturnExpDate() {
		Method method = getMethodPrivateToMethodPublic("getExpDate", String.class);
		Date issueDate = new Date(); // now
		String token = jwtUtil.getToken("hello", issueDate);
		Date expDateFromMethod = null;
		try {
			expDateFromMethod = (Date) method.invoke(jwtUtil, token);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.getCause().printStackTrace();
		}
		if (expDateFromMethod == null)
			fail("token is null");
		assertTrue(issueDate.before(expDateFromMethod));
	}

	@Test // Unit test
	public void whenGiveInvalidToken_thenReturnNullExpDate() {
		Method method = getMethodPrivateToMethodPublic("getExpDate", String.class);
		String token = "inv" + jwtUtil.getToken("hello"); // Invalid token
		Date expDateFromMethod = null;
		try {
			expDateFromMethod = (Date) method.invoke(jwtUtil, token);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.getCause().printStackTrace();
		}
		assertNull(expDateFromMethod);
	}

	@Test // Unit test
	public void whenGiveACorrectToken_thenReturnIssDate() {
		Method method = getMethodPrivateToMethodPublic("getIssDate", String.class);
		Date issueDate = new Date(); // now
		String token = jwtUtil.getToken("hello", issueDate);
		Date dateFromMethod = null;
		try {
			dateFromMethod = (Date) method.invoke(jwtUtil, token);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.getCause().printStackTrace();
		}
		if (dateFromMethod == null)
			fail("token is null");
		assertTrue(issueDate.toString().equals(dateFromMethod.toString()));
	}

	@Test // Unit test
	public void whenGiveInvalidToken_thenReturnNullIssDate() {
		Method method = getMethodPrivateToMethodPublic("getIssDate", String.class);
		String token = "inv" + jwtUtil.getToken("hello"); // Invalid token
		Date issDateFromMethod = null;
		try {
			issDateFromMethod = (Date) method.invoke(jwtUtil, token);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.getCause().printStackTrace();
		}
		assertNull(issDateFromMethod);
	}
	
	private Method getMethodPrivateToMethodPublic(String nameMethod, Class<?>... param) {
		Method method = null;
		try {
			method = JwtUtil.class.getDeclaredMethod(nameMethod, param);
			method.setAccessible(true);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		return method;
	}
	
	@Test
	public void whenTokenIsValid_thenReturnTrue() {
		assertTrue(jwtUtil.tokenIsValid(jwtUtil.getToken("hello")));
	}
	
	@Test
	public void whenTokenIsExpired_thenReturnFalse() throws ParseException {
		Date expDate = new java.text.SimpleDateFormat("dd-MM-yyyy").parse("30-07-2000");
		assertFalse((jwtUtil.tokenIsValid(jwtUtil.getToken("hello",expDate))));
	}
	
	@Test
	public void whenTokenIsNull_thenExceptionThrown() throws ParseException {
		Exception e = assertThrows(IllegalArgumentException.class, () -> {
			jwtUtil.tokenIsValid(null);
	    });
	    assertTrue(e.getMessage().contains("cannot be null"));
	}
	
	@Test
	public void whenTokenIsOk_thenReturnEmail() {
		String email = "email@gmail.com";
		String token = jwtUtil.getToken(email);
		assertEquals(email, jwtUtil.getEmail(token));
	}
	
	@Test
	public void whenEmailIsNull_thenReturnNull() {
		Exception e =  assertThrows(IllegalArgumentException.class, 
				() -> jwtUtil.getEmail(null)
		);
		assertTrue(e.getMessage().contains("cannot be null"));
	}

}
