package com.openset.openauth.account;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.openset.openauth.model.Account;
import com.openset.openauth.model.AuthenticationRequest;
import com.openset.openauth.model.AuthenticationResponse;

@SpringBootTest
public class AccountModelTest {
	
	@Test
	public void accountModelIsOk() {
		Long id = 9999L;
		Account account = new Account(); 
		account.setId(id);
		assertEquals(id, account.getId());
	}
	
	@Test
	public void authenticationReqTestIsOk() {
		AuthenticationRequest authRequest = new AuthenticationRequest();
		authRequest.setEmail("test@gmail.com");
		authRequest.setPassword("passw");
		assertEquals("test@gmail.com", authRequest.getEmail());
		assertEquals("passw",authRequest.getPassword());
	}
	
	@Test
	public void authenticationResTestIsOk() {
		String jwtToken = "jwt_token";
		AuthenticationResponse authResponse = new AuthenticationResponse(jwtToken);
		assertEquals(jwtToken, authResponse.getJwt());
	}
}
