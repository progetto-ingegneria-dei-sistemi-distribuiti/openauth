package com.openset.openauth.account;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.openset.openauth.model.Account;
import com.openset.openauth.repository.AccountRepository;
import com.openset.openauth.service.AccountService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class AccountServiceIntegrationTest {
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private AccountRepository accountRepository;


	@Test
	public void whenSaveAccount_thenDbIncreaseByOne() {
		Long numberOfRaw = accountRepository.count();
		Account account = new Account();
		account.setEmail("test@test.com");
		account.setPassword("passw");
		
		accountService.saveAccount(account);
		assertTrue(accountRepository.count() == numberOfRaw+1);
	}
	
	@Test
	public void whenSaveAccountWithDuplicateEmailInDb_thenReturnFalse() {
		Account account01 = new Account();
		account01.setEmail("test01@test.com");
		account01.setPassword("passw");
		Account account02 = new Account();
		account02.setEmail("test01@test.com");
		account02.setPassword("passw");
		
		assertAll(
				() -> assertTrue(accountService.saveAccount(account01)),
				() -> assertFalse(accountService.saveAccount(account02))
			);
	}
	
	@Test
	public void whenFindByEmailInService_thenReturnAccount() {
		Account account = new Account();
		account.setEmail("test02@test.com");
		account.setPassword("passw");
		if(!accountService.saveAccount(account)) fail();
		Account foundFromDb = accountService.getAccountByEmail(account.getEmail());
		assertTrue(foundFromDb.getId().longValue() == account.getId().longValue());
	}
	
	@Test
	public void whenFindByEmailInService_thenReturnNull() {
		Account foundFromDb = accountService.getAccountByEmail("doesnotexit@test.com");
		assertNull(foundFromDb);
	}
}
