package com.openset.openauth.account;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.openset.openauth.model.Account;
import com.openset.openauth.service.AccountService;
import com.openset.openauth.service.MyUserDetailsService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class MyUserDetailsServiceTest {
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private MyUserDetailsService myUserDetailsService;
	
	@Test
	public void whenEmailIsOk_thenReturnUserDetailsOk() {
		String email = "testttt@test.com";
		String passw = "passw";
		Account account = new Account();
		account.setEmail(email);
		account.setPassword(passw);
		accountService.saveAccount(account);
		
		assertEquals(email,myUserDetailsService.loadUserByUsername(email).getUsername());
		assertEquals(passw,myUserDetailsService.loadUserByUsername(email).getPassword());
	}
	
	
}
