package com.openset.openauth.account;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.openset.openauth.model.Account;
import com.openset.openauth.repository.AccountRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(locations = "classpath:application-test-repository.properties")
public class AccountRepositoryIntegrationTest {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Test
	public void givenACorrectSetup_thenAnEntityManagerWillBeAvailable() {
		assertNotNull(entityManager);
	}
	@Test
	public void givenACorrectSetup_thenAnAccountRepositoryWillBeAvailable() {
		assertNotNull(entityManager);
	}
	
	@Test
	public void whenFindByEmail_thenReturnAccount() {
		Account accountTest00 = new Account();
		accountTest00.setEmail("test00@test.com");
		accountTest00.setPassword("passw");
		entityManager.persistAndFlush(accountTest00);
		accountRepository.save(accountTest00);
		Account found = accountRepository.findByEmail(accountTest00.getEmail()).get();
		assertTrue(found.getEmail().equals(accountTest00.getEmail()));
	}
	
	@Test
	public void whenInvalidEmail_thenReturnNull() {
		Account fromDb = accountRepository.findByEmail("doesnotexist@test.com").orElse(null);
		assertNull(fromDb);
	}
	
	@Test
	public void whenFindById_thenReturnAccount() {
		Account accountTest01 = new Account();
		accountTest01.setEmail("test01@test.com");
		accountTest01.setPassword("passw");
		entityManager.persistAndFlush(accountTest01);
		
		Account fromDb = accountRepository.findById(accountTest01.getId()).get();
		assertTrue(fromDb.getEmail().equals(accountTest01.getEmail()));
	}
	
	@Test
	public void whenInvalidId_thenReturnNull() {
		Account fromDb = accountRepository.findById(-1L).orElse(null);
		assertNull(fromDb);
	}
	
	@Test
    public void givenSetOfAccounts_whenFindAll_thenReturnAllAccounts() {
		Account test02 = new Account(); 
		test02.setEmail("test02@test.com");
		test02.setPassword("passw");
		
		Account test03 = new Account(); 
		test03.setEmail("test03@test.com");
		test03.setPassword("passw");
		
		Account test04 = new Account(); 
		test04.setEmail("test04@test.com");
		test04.setPassword("passw");
		
		entityManager.persist(test02);
		entityManager.persist(test03);
		entityManager.persist(test04);
		entityManager.flush();
		
		List<Account> allAccount = accountRepository.findAll();
		assertAll(
				() -> assertTrue(allAccount.get(0).getEmail().equals(test02.getEmail())),
				() -> assertTrue(allAccount.get(1).getEmail().equals(test03.getEmail())),
				() -> assertTrue(allAccount.get(2).getEmail().equals(test04.getEmail()))
			);
	}
	
}
