package com.openset.openauth.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import com.openset.openauth.jwt.JwtUtil;
import com.openset.openauth.model.Account;
import com.openset.openauth.model.AuthenticationRequest;
import com.openset.openauth.model.AuthenticationResponse;
import com.openset.openauth.service.AuthenticatorService;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class AuthenticatorServiceTest {
	
	@Autowired
	private AuthenticatorService authenticatorService; 
	
	@Autowired
	private JwtUtil jwtUtil; 

	@Test
	public void whenRegisterCorrectAccount_thenReturnAccount() {
		Account account = new BuilderAccount().setEmail("test@service2.com").setPassword("passw").build();
		ResponseEntity<?> response = authenticatorService.getRegisterNewUserResponse(account);
		
		assertEquals(200, response.getStatusCodeValue());
		
		Account expectResponse = new BuilderAccount().setEmail("test@service2.com").setPassword("").build();
		Account accountResponse = (Account) response.getBody(); 
		
		assertEquals(expectResponse.getEmail(),accountResponse.getEmail());
		assertEquals(expectResponse.getPassword(),accountResponse.getPassword());
		
	}
	
	@Test
	public void whenLoginWithCorrectAccount_thenReturnJWTResponse() {
		Account account = new BuilderAccount().setEmail("test@service4.com").setPassword("passw").build();
		ResponseEntity<?> response = authenticatorService.getRegisterNewUserResponse(account);
		assertEquals(200, response.getStatusCodeValue());
		AuthenticationRequest authenticationRequest = new AuthenticationRequest();
		authenticationRequest.setEmail(account.getEmail());
		authenticationRequest.setPassword("passw");
		ResponseEntity<?> resp = authenticatorService.getRespAuthenticationToken(authenticationRequest);
		AuthenticationResponse responseJwt = (AuthenticationResponse) resp.getBody(); 
		assertTrue(jwtUtil.tokenIsValid(responseJwt.getJwt())); // valid token
		assertEquals(200,resp.getStatusCodeValue());
	}
	
	@Test
	public void whenLoginWithBadPassword_thenReturnErrorResponse() {
		Account account = new BuilderAccount().setEmail("test@service5.com").setPassword("passw").build();
		ResponseEntity<?> response = authenticatorService.getRegisterNewUserResponse(account);
		assertEquals(200, response.getStatusCodeValue());
		AuthenticationRequest authenticationRequest = new AuthenticationRequest();
		authenticationRequest.setEmail(account.getEmail());
		authenticationRequest.setPassword("WRONG_PASSWORD");
		ResponseEntity<?> resp = authenticatorService.getRespAuthenticationToken(authenticationRequest);
		String responseWithBadPasswordError = (String) resp.getBody(); 
		assertTrue(responseWithBadPasswordError.contains("Access Denied"));
		assertEquals(403,resp.getStatusCodeValue());		
	}
	
	@Test
	public void whenLoginWithNotExistEmail_thenReturnErrorResponse() {
		Account account = new BuilderAccount().setEmail("test@service6.com").setPassword("passw").build();
		ResponseEntity<?> response = authenticatorService.getRegisterNewUserResponse(account);
		assertEquals(200, response.getStatusCodeValue());
		AuthenticationRequest authenticationRequest = new AuthenticationRequest();
		authenticationRequest.setEmail("wrong@email.com");
		authenticationRequest.setPassword("passw");
		ResponseEntity<?> resp = authenticatorService.getRespAuthenticationToken(authenticationRequest);
		String responseWithBadEmailError = (String) resp.getBody(); 
		assertTrue(responseWithBadEmailError.contains("Email"));
		assertEquals(403,resp.getStatusCodeValue());		
	}
	
	@Test
	public void whenRegisterDuplicateEmail_thenReturnError() {
		Account account = new BuilderAccount().setEmail("test@service3.com").setPassword("passw").build();
		ResponseEntity<?> response = authenticatorService.getRegisterNewUserResponse(account);
		assertEquals(200, response.getStatusCodeValue());
		Account accountFail = new BuilderAccount().setEmail("test@service3.com").setPassword("passw").build();
		ResponseEntity<?> responseFail = authenticatorService.getRegisterNewUserResponse(accountFail);
		assertEquals(502, responseFail.getStatusCodeValue());
		assertTrue(responseFail.getBody().toString().contains("already exists"));
	}
	
	@Test
	public void whenBodyRequestIsEmpty_thenReturnErrorResponse() {
		AuthenticationRequest authenticationRequest = new AuthenticationRequest();
		ResponseEntity<?> resp = authenticatorService.getRespAuthenticationToken(authenticationRequest);
		assertEquals(403,resp.getStatusCodeValue());		
	}
	
	
	private class BuilderAccount {
		private Account account;
		BuilderAccount() { account = new Account(); }
		public BuilderAccount setEmail(String email) {
			account.setEmail(email);
			return this; 
		}
		public BuilderAccount setPassword(String password) {
			account.setPassword(password);
			return this; 
		}
		public Account build() {
			return account; 
		}
	}
	
	
}
