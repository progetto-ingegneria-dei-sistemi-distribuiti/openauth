package com.openset.openauth.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openset.openauth.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long>{
	
	public Optional<Account> findById(Long id);
	public Optional<Account> findByEmail(String email);

}
