package com.openset.openauth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.openset.openauth.model.Account;
import com.openset.openauth.model.AuthenticationRequest;
import com.openset.openauth.service.AuthenticatorService;

@RestController
public class AuthenticatorController {

	@Autowired
	private AuthenticatorService authenticatorService;

	@PostMapping(value = "/authentication")
	public ResponseEntity<?> getAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) {
		return authenticatorService.getRespAuthenticationToken(authenticationRequest);	
	}
	
	@PostMapping(value = "/register") 
	public ResponseEntity<?> registerNewUser(@RequestBody Account newAccount) {
		return authenticatorService.getRegisterNewUserResponse(newAccount);
	}
	
	@PostMapping(value = "/validate")
	public ResponseEntity<?> validateToken(@RequestHeader("Authorization") String token) {
		return ResponseEntity.ok(true);
	}

}
