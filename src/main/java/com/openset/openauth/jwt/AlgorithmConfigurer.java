package com.openset.openauth.jwt;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.auth0.jwt.algorithms.Algorithm;

@Configuration
public class AlgorithmConfigurer {
	
	@Bean
	public Algorithm concreteAlgorithm(@Value("${jwt.security.strategy}") String strategy, 
			@Value("${jwt.security.secret}") String secret) {
		switch (strategy) {
			case "HS256": return Algorithm.HMAC256(secret); 
			case "HS384": return Algorithm.HMAC384(secret);
			case "HS512": return Algorithm.HMAC512(secret);
			default: return Algorithm.HMAC256(secret);
		}
	}
	
	@Bean
	public JwtUtil jwtUtilBean() {
		return new JwtUtil();
	}
	
}
