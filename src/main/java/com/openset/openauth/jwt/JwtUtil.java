package com.openset.openauth.jwt;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

@Service
public class JwtUtil {
	
	@Autowired
	private Algorithm alg;
	
	@Value("${jwt.token.exp.minutes:60}")
	private String minutes;
	
	@Value("${jwt.token.exp.hours:1}")
	private String hours;
	
	@Value("${jwt.token.issue:openauth}")
	private String issuer;
	
	private long getTokenExpire() {
		return 1000L * Long.parseLong(minutes) * 60 * Long.parseLong(hours);
	}
	
	public String getToken(String subject) {
		return getToken(subject,new Date());
	}
	
	public String getToken(String subject, Date start) { // Util for testing
		String token = JWT.create()
				.withSubject(subject)
				.withIssuedAt(start)
				.withExpiresAt(new Date(start.getTime()+getTokenExpire()))
				.withIssuer(issuer)
				.sign(alg);
		return token;
	}
	
	private Optional<DecodedJWT> getDecodedToken(String token) {
		Optional<DecodedJWT> jwt;
		try {
			jwt = Optional.of(JWT.decode(token));
		} catch (JWTDecodeException | NullPointerException exception ) {
			jwt = Optional.ofNullable(null); 
		}
		return jwt;
	}
	
	private String getSubject(String token) {
		Optional<DecodedJWT> jwt = getDecodedToken(token);
		if (jwt.isPresent())
			return jwt.get().getSubject();
		throw new IllegalArgumentException("The token; cannot be null");
	}
	
	public String getEmail(String token) {
		String email = getSubject(token);
		if (email == null) throw new IllegalArgumentException("The token; cannot be null");
		return email;
	}
	
	private Date getExpDate(String token) {
		Optional<DecodedJWT> jwt = getDecodedToken(token);
		if (jwt.isPresent())
			return jwt.get().getExpiresAt();
		throw new IllegalArgumentException("The token; cannot be null");
	}
	
	private Date getIssDate(String token) {
		Optional<DecodedJWT> jwt = getDecodedToken(token);
		if (jwt.isPresent())
			return jwt.get().getIssuedAt();
		throw new IllegalArgumentException("The token; cannot be null");
	}
	
	public boolean tokenIsValid(String token) {
		Date expDate = getExpDate(token);
		if (expDate.before(new Date()))
			return false;
		return true;
	}
	
	
	
}
