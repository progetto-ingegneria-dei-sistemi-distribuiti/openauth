package com.openset.openauth.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.openset.openauth.jwt.JwtUtil;
import com.openset.openauth.model.Account;
import com.openset.openauth.model.AuthenticationRequest;
import com.openset.openauth.model.AuthenticationResponse;

@Service	
public class AuthenticatorService {
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private MyUserDetailsService userDetailsService;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private AccountService accountService;
	
	
	public ResponseEntity<?> getRespAuthenticationToken(AuthenticationRequest authenticationRequest) {
		Optional<String> myServiceResponse = authenticationToken(authenticationRequest);
		String myStringResponse = myServiceResponse.get(); 
		if (myStringResponse.contains("does not exist") || myStringResponse.contains("Wrong Password"))
			return ResponseEntity.status(403).body(myStringResponse);
		
		return ResponseEntity.ok(new AuthenticationResponse(myStringResponse)); // jwt
	}
	
	private Optional<String> authenticationToken(AuthenticationRequest authenticationRequest) {
		Optional<String> authToken = Optional.ofNullable(null); 
		if (accountService.getAccountByEmail(authenticationRequest.getEmail()) == null) 
			return authToken = Optional.of("Email does not exist");
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getEmail(), authenticationRequest.getPassword()));
		} catch (BadCredentialsException e) {
			return authToken = Optional.of("Access Denied - Wrong Password");
		}
		UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
		authToken = Optional.of(jwtUtil.getToken(userDetails.getUsername()));
		return authToken; 
	}
	
	public ResponseEntity<?> getRegisterNewUserResponse(Account newAccount) {
		newAccount.setPassword(encoder.encode(newAccount.getPassword()));
		if(!accountService.saveAccount(newAccount)) return ResponseEntity.status(502).body("Email already exists");
		newAccount.setPassword("");
		return ResponseEntity.ok(newAccount);
	}
}
