package com.openset.openauth.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openset.openauth.model.Account;
import com.openset.openauth.repository.AccountRepository;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;
	
	public Account getAccountByEmail(String email) {
		Optional<Account> account = accountRepository.findByEmail(email);
		if (account.isPresent()) return account.get();
		return null;
	}
	
	public boolean saveAccount(Account account) {
		if (getAccountByEmail(account.getEmail()) != null) return false;
		accountRepository.save(account);
		return true;
	}
	
}
