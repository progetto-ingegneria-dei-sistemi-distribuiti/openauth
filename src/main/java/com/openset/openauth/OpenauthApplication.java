package com.openset.openauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenauthApplication.class, args);
	}

}
